# Starter Template

## Requirements

Install `pnpm`:

```sh
# Standalone script
curl -fsSL https://get.pnpm.io/install.sh | sh -
# Install using npm
npm install -g pnpm
```

## Scripts

### `pnpm cz`

Commit using [commitizen](https://github.com/commitizen-tools/commitizen) as helper to follow [Conventional Commits](https://www.conventionalcommits.org/) rules.

### `pnpm format`

Format code with [Prettier](https://prettier.io/) applying the rules in `.prettierrc`.

### `pnpm prepare`

Install [Husky](https://github.com/typicode/husky) hooks into the `.husky\` folder.

### `pnpm release`

Create a new version for the package using [Conventional Changelog](https://github.com/conventional-changelog/conventional-changelog) and auto-generate a `CHANGELOG.md` file.

## Utilities

### Sort `package.json`

```sh
pnpx sort-package-json
```
